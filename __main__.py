from http.server import SimpleHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs
from argparse import ArgumentParser
from pprint import pprint

import threading
import subprocess
import json
import os
import re

# Command-line arguments
parser = ArgumentParser()
parser.add_argument('-vh', '--video-height', type = int, default = 720, help = 'The desired height of any video fetched with yt-dlp')
parser.add_argument('-sl', '--subtitle-language', type = str, default = 'en', help = 'The desired subtitle language')
parser.add_argument('-yt', '--yt-dlp-location', type = str, help = 'Location of the yt-dlp binary')
parser.add_argument('-pc', '--player-command', type = str, default = 'xdg-open FILE', help = 'Command to open media files with')
args = parser.parse_args()

base_dir = os.path.dirname(os.path.realpath(__file__))

def open_file(url):
    if 'http' in url and args.yt_dlp_location:
        os.system(' '.join([
            args.yt_dlp_location,
            '--format-sort', '"height:' + str(args.video_height) + '"',
            '--output', '"/tmp/hurl/%(title)s.%(ext)s"',
            '--sub-langs', args.subtitle_language,
            '--write-subs',
            '--write-auto-subs',
            url,
            ' > /var/log/hurl.log'
        ]))

        log_file = open('/var/log/hurl.log', 'r')
        log = log_file.read()
        log_file.close()

        # TODO: Search for "Unsupported URL"

        file_paths = re.findall(r'(\/.*\.[webm|mp4|avi|mkv|mov]+)[ \n]', log)

        if len(file_paths) > 0:
            os.system(args.player_command.replace('FILE', file_paths[0]))

    else:
        os.system(args.player_command.replace('FILE', url))

class Server(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory = base_dir, **kwargs)
    
    def do_GET(self):
        path = urlparse(self.path).path
        query = parse_qs(urlparse(self.path).query)

        if path != '/':
            super().do_GET()
            return

        # API input request
        if 'input' in query and query['input'][0]:
            url = query['input'][0].replace('youtu.be', 'youtube.com')

            t = threading.Thread(target = open_file, args = [ url ])
            t.daemon = True
            t.start()

            # Request was submitted via the form, return the user to the UI
            if 'redirect' in query:
                self.send_response(301)
                self.send_header('Location', '/')
                self.end_headers()

            # Request was submitted remotely, send a plain text confirmation
            else:
                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                   
                self.wfile.write(bytes('Playing ' + url, 'utf-8'))

        # Return the yt-dlp log
        elif 'log' in query:
            self.send_response(200)
            self.send_header('Content-type', 'text/plain')
            self.end_headers()
               
            log = ''

            if os.path.exists('/var/log/hurl.log'):
                with open('/var/log/hurl.log', 'r') as log_file:
                    log = log_file.read()
                    log_file.close()
            
            self.wfile.write(bytes(log, 'utf-8'))

        # Render the UI
        else:
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()

            with open(base_dir + '/template.html', 'r') as html_file:
                html = html_file.read()
                html_file.close()

                self.wfile.write(bytes(html, 'utf-8'))

if __name__ == '__main__':        
    server = HTTPServer(('0.0.0.0', 8888), Server)
    print('Server started')
    
    if not os.path.isdir('/tmp/hurl'):
        os.mkdir('/tmp/hurl')

    try:
        server.serve_forever()

    except KeyboardInterrupt:
        pass

    server.server_close()
    print('Server stopped')
